const User = require("./model/user");

// create a new user called Yossi
let user = new User({
  name: 'Yossi',
  username: 'Levi',
  password: '12345' 
});

// call the custom method. this will just add -dude to his name
// user will now be Chris-dude
user.dudify(function(err, name) {
  if (err) throw err;
  console.log('Your new name is ' + name);
});

// call the built-in save method to save to the database
user.save(function(err) {
  if (err) throw err;
  console.log('User saved successfully!');
});

// create more users
User({
  name: 'Dani Zilberstein',
  username: 'dzilbers',
  password: 'danidani',
  admin: true
}).save(function(err) {
  if (err) throw err;
  console.log('User created!');
});
User.CREATE({
  name: 'Shmuel-Zvi Naiman',
  username: 'sznaiman',
  password: 'naiman',
  admin: true
}, function(err, user) {
  if (err) throw err;
  console.log('User created:' + user);
});





